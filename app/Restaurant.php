<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $fillable = array('name', 'address', 'cuisine');

    protected $hidden = array('created_at', 'updated_at');

    public function menus() {
        return $this->hasMany('App\Menu');
    }
}
