<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = array('restaurant_id', 'date', 'description', 'price');

    protected $hidden = array('created_at', 'updated_at');

    public function restaurant() {
        return $this->belongsTo('App\Restaurant');
    }

    public function dish() {
        return $this->hasMany('App\Dish');
    }
}
