<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    protected $fillable = array('menu_id', 'description', 'allergies');

    public function menu() {
        $this->belongsTo('App\Menu');
    }
}
