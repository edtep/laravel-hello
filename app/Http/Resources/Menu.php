<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Menu extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->when($request->path() == 'api/menus', $this->id),
            'restaurant_id' => $this->when($request->path() == 'api/menus', $this->restaurant_id),
            'date' => $this->date,
            'description' => $this->description,
            'price' => $this->price
        ];
    }
}
